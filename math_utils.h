/**
 * @file
 * @author Martin Stejskal
 * @brief Extra math functionality which might be handy
 */
#ifndef __MATH_UTILS_H__
#define __MATH_UTILS_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Calculate coefficients for linear approximation
 *
 * This might be handy when comes to calibration ADC. Simply we know ideal
 * values, but real values on ADC might differ (a lot). To reduce absolute
 * error a linear approximation might be used. Simply with some precise
 * voltmeter and good power supply correct voltage is set and then real value
 * is sampled. Since it is known what value on ADC ideally should be, it is
 * possible to generate function which will correct error in given range of
 * values.
 *
 * To get "ideal value" from "real value", simply use
 * @ref linearApproxGetCorrectedValue function
 *
 * @param[in] f_ideal_min Ideal minimum value
 * @param[in] f_ideal_max Ideal maximum value
 * @param[in] f_real_min Measured (real) minimum value
 * @param[in] f_real_max Measured (real) maximum value
 * @param[out] pf_coef_offset Output offset coefficient
 * @param[out] pf_coef_mul Output multiplier coefficient
 */
void mu_linear_approx_get_coef_float(float f_ideal_min, float f_ideal_max,
                                     float f_real_min, float f_real_max,
                                     float *pf_coef_offset, float *pf_coef_mul);

/**
 * @brief Calculate "ideal value" from real value by using linear approximation
 *
 * @param[in] f_real_value Real (measured) value
 * @param[in] f_coef_offset Offset coefficient obtained by
 *                    @ref mu_linear_approx_get_coef_float
 * @param[in] f_coef_mul Multiplier coefficient obtained by
 *                    @ref mu_linear_approx_get_coef_float
 * @return Ideal value which corresponds with idealized line
 */
float mu_linear_approx_get_corrected_value_float(float f_real_value,
                                                 float f_coef_offset,
                                                 float f_coef_mul);

/**
 * @brief Calculate coefficients for linear approximation
 *
 * Analogy to @ref linearApproxGetCoefFloat but for 16 bit unsigned values
 *
 * @param[in] u16_ideal_min Ideal minimum value
 * @param[in] u16_ideal_max Ideal maximum value
 * @param[in] u16_real_min Measured (real) minimum value
 * @param[in] u16_real_max Measured (real) maximum value
 * @param[out] pi16_coef_offset Output offset coefficient
 * @param[out] pi32_coef_mul Output multiplier coefficient
 */
void mu_linear_approx_get_coef_u16(uint16_t u16_ideal_min,
                                   uint16_t u16_ideal_max,
                                   uint16_t u16_real_min, uint16_t u16_real_max,
                                   int16_t *pi16_coef_offset,
                                   int32_t *pi32_coef_mul);

/**
 * @brief Calculate "ideal value" from real value by using linear approximation
 *
 * @param[in] u16_real_value
 * @param[in] i16_coef_offset
 * @param[in] i32_coef_mul
 * @return Ideal value which corresponds with idealized line
 */
uint16_t mu_linear_approx_get_corrected_value_u16(uint16_t u16_real_value,
                                                  int16_t i16_coef_offset,
                                                  int32_t i32_coef_mul);
#endif  // __MATH_UTILS_H__
